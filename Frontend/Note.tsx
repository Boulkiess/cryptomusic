import { Observable, Subject, Subscription } from 'rxjs';

export class Note {
    private static noteCtr = 0;

    private stopWatcher$: Subject<void> = new Subject<void>();
    private waveform = 'sawtooth';
    private gainNode: GainNode;
    private noteCtr: number;

    private noteNumber: number;
    private frequency: number;

    /*enveloppe*/
    private attack: number = 0;
    private decay: number = 0.3;
    private sustain: number = 1.0;
    private release: number = 0.1;

    private audioCtx: AudioContext;
    private osc: any;

    constructor(
        noteNumber: number,
        audioCtx: AudioContext,
        outputBus: GainNode
    ) {
        this.noteNumber = noteNumber;
        this.noteCtr = Note.noteCtr;
        Note.noteCtr++;
        this.audioCtx = audioCtx;
        this.gainNode.connect(outputBus);
        this.gainNode = this.audioCtx.createGain();
        this.osc = this.audioCtx.createOscillator();
        this.osc.connect(this.gainNode);
        this.frequency = 440 * Math.pow(2, ((noteNumber - 69) / 12));
    }

    play() {
        const now = this.audioCtx.currentTime;

        /*set frequency and waveform*/
        this.osc.frequency.value = this.frequency;
        this.osc.type = 'sawtooth';

        this.gainNode.gain.value = 0.2;

        /*gain ADSR enveloppe*/
        this.gainNode.gain.linearRampToValueAtTime(1, now + this.attack);
        this.gainNode.gain.setTargetAtTime(1 / 2, now + this.attack + this.sustain, this.decay);
        this.gainNode.gain.setTargetAtTime(0, now + this.attack + this.decay + this.sustain, this.release);

        this.osc.start();
    }

    stop() {
        const now = this.audioCtx.currentTime;
        this.gainNode.gain.cancelScheduledValues(0);
        this.gainNode.gain.setTargetAtTime(0, now, this.release);
        Note.noteCtr--;
    }

}