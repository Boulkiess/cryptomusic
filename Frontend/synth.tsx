export interface SynthMessage { }
export class SynthNoteMessage implements SynthMessage {
    readonly noteNumber: number;
    constructor(noteNumber: number) {
        this.noteNumber = noteNumber;
    }
}
export class SynthNoteOn extends SynthNoteMessage {
    constructor(props) {
        super(props);
    }
}
export class SynthNoteOff extends SynthNoteMessage { }
export class TriggerSample implements SynthMessage {
    public instrument: string;
    public velocity: number;
    constructor(instrument: string, velocity: number) {
        this.instrument = instrument;
        this.velocity = velocity;
    }
}
export class VolumeChange {
    public level: number;
    constructor(level: number) {
        // hack due to arduino stupidity kenny
        this.level = Math.min(level / 127.0);
    }
}
export class WaveformChange {
    public waveForm: string;
    constructor(public rawValue: number) {
        switch (rawValue) {
            case 0:
                this.waveForm = 'sawtooth';
                break;
            case 1:
                this.waveForm = 'sine';
                break;
            case 2:
                this.waveForm = 'triangle';
                break;
            case 3:
                this.waveForm = 'square';
                break;
            default:
                this.waveForm = 'sawtooth';
        }
    }
}