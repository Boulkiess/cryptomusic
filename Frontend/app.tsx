declare var require: any

var React = require('react');
var ReactDOM = require('react-dom');
//import * as webmidi from "webmidi";
//import WebMidi from "webmidi";
var WebMidi = require('webmidi');

export class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
        //this.noteToFreq = this.noteToFreq.bind(this);
    }

    noteToFreq = (note) => {
        const freq = 440 * Math.pow(2, ((note - 69) / 12));
        return freq;
    }

    /*playNote(frequency, attack, decay, sustain, release) {
        const audioCtx = this.state.audioCtx;
        const osc = audioCtx.createOscillator();
        const notes = this.state.notes || {};
        const oscs = this.state.oscs || {};

        *//*set frequency and waveform*//*
        osc.frequency.value = frequency;
        osc.type = 'sawtooth';

        *//*set gain*//*
        const gain = audioCtx.createGain();
        gain.value = 0.2;
        gain.connect(this.state.m asterGain);
        osc.connect(gain);

        *//*gain enveloppe*//*
        gain.gain.linearRampToValueAtTime(1, attack);
        gain.gain.setTargetAtTime(1 / 2, attack + sustain, decay);

        notes[frequency] = notes[frequency] > 1 ? notes[frequency] + 1 : 1;
        oscs[frequency] = osc;
        this.setState({ notes: notes });
        this.setState({ oscs: oscs }, () => {
            osc.start();
        });

        *//*subscribe to note stop*//*
        const subscription = 
        
    }*/

    stopNote(frequency) {
        const notes = this.state.notes;
        const oscs = this.state.oscs;
        if (notes[frequency]) {
            notes[frequency]--;
            this.setState({ notes: notes });
            if (oscs[frequency] && notes[frequency] <= 0) {
                oscs[frequency].stop();
            }
        }
    }

    refreshContext() {
        const audioCtx = new (window.AudioContext || window.webkitAudioContext)();
        const masterGain = audioCtx.createGain();
        masterGain.gain.value = 1;
        masterGain.connect(audioCtx.destination);
        this.setState({ audioCtx: audioCtx, masterGain: masterGain });
        WebMidi.enable((err) => {
            if (err) {
                console.log("WebMidi could not be enabled.", err);
            } 

            console.log("WebMidi enabled!");
            const input = WebMidi.getInputByName("virtual");
            this.setState({ input: input });
            console.log("Connected to midi input", this.state.input);

            /*Subscribe to note_on notifications*/
            this.state.input.addListener('noteon', "all", (e) => {
                const frequency = this.noteToFreq(e.note.number);
                this.playNote(frequency);
            });

            /*Subscribe to note_off notifications*/
            this.state.input.addListener('noteoff', "all", (e) => {
                const frequency = this.noteToFreq(e.note.number);
                if (frequency) {
                    this.stopNote(frequency);
                }
            });
            

        });
    }

    componentDidMount() {
        this.refreshContext();
    }

    render() {
        return (
            <div className="App">
                SYNTH TO COME
            </div>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('root'));