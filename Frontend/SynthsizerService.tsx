declare var require: any

var React = require('react');
import { Note } from 'synth';

export class SynthesizerService {
    // hold onto our notes once created
    private notes: Note[];
    setup(audioContext: Context, synthStream$: Subject<SynthMessage>,
        audioBus: AudioNode) {
        // start by setting static values for all notes
        Note.configure(audioContext, synthStream$, audioBus);
        // now, configure each note object
        this.notes = [
            new Note(['C0'], 16.3516),
            new Note(['C#0', 'Db0'], 17.3239),
            new Note(['D0'], 18.3540),
            new Note(['D#0', 'Eb0'], 19.4454),
            ...
            new Note(['D8'], 4698.64),
            new Note(['D#8', 'Eb8'], 4978.03),
            new Note(['E8'], 5274.041),
            new Note(['F8'], 5587.652)
        ];
    }
}