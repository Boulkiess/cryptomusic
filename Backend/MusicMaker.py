#!/usr/bin/env python3
# coding=utf-8

from typing import Dict, Tuple, Sequence
import mido
import asyncio
import time


class MusicMaker(object):
    def __init__(self, scale: Tuple[int, Tuple(int)] = None, port: str = None):
        self.scale = scale if scale else (
            21, (0, 2, 3, 5, 7, 8, 10, 12))  # A Major scale
        self.outport = None
        self.midi_port = port if port else mido.get_output_names()[-1]
        print(self.outport)

    async def play_note(self, note: int, velocity: int, length: int) -> None:
        """
        Send a midi message asking to play a note with a given velocity for a given time.
        """
        m_on = mido.Message('note_on', note=note, velocity=64, time=length)
        self.outport.send(m_on)

        await asyncio.sleep(length / 1000)

        m_off = mido.Message('note_off', note=note)
        self.outport.send(m_off)

    async def make_music(self, data: str = None, n_notes: int = None, length: int = None) -> None:
        """
        Send midi messages corresponding to a chord of given length
        with notes chosen by an arbitrary parsing technique applied on a 
        hexadecimal stream.
        """
        if data is None:
            return
        n_notes = n_notes if n_notes and 0 < n_notes <= 16 else 1
        length = length if length else 1

        chord = []
        for n in range(n_notes):
            # 0 -> 8: octave
            o = int(data[n], 16) // 2
            # 0 -> 8: interval
            i = int(data[n + 1], 16) // 2
            # midi note
            m = 12 * o + self.scale[0] + self.scale[1][i]
            # 0 -> 127: velocity
            v = int(data[n+2:n+4], 16) // 2

            chord.append((m, v))

        chord.sort()
        asyncio.gather(*[
            self.play_note(note, vel, length)
            for note, vel in chord
        ])

    @property
    def midi_port(self) -> None:
        """The midi_port property."""
        return self._midi_port

    @midi_port.setter
    def midi_port(self, value: str) -> None:
        if self.outport and not self.outport.closed:
            self.outport.close()
        self._midi_port = value
        self.outport = mido.open_output(self._midi_port)


if __name__ == "__main__":
    maker = MusicMaker()
