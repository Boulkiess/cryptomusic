#!/usr/bin/env python3

import asyncio
import json

import websockets

from MusicMaker import MusicMaker


class CryptoMusic(object):
    """
    An Asynchronous class the generate music based on blockchain events.
    """

    def __init__(self, uri: str = None):
        self.uri = uri if uri else "wss://ws.blockchain.info/inv"
        self.maker = MusicMaker()

    def start(self):
        """
        Start listening to the websocket.
        """
        asyncio.get_event_loop().run_until_complete(self.connect())

    async def connect(self):
        """
        Connect to the websocket and subscribe to notifications.
        """
        async with websockets.connect(self.uri) as websocket:
            # subscribe to new transactions
            message = '{"op":"unconfirmed_sub"}'
            await websocket.send(message)

            # subscribe to new blocks
            message = '{"op":"blocks_sub"}'
            await websocket.send(message)

            # handle messages
            await self._consumer_handler(websocket)

    async def _play(self, data: json) -> None:
        """
        Arbitrarily parse JSON data to determine musical properties to generate.
        """
        size = data['x']['size']
        time = data['x']['time']
        hashcode = data['x']['hash']
        vin = data['x']['vin_sz']
        vout = data['x']['vout_sz']

        await self.maker.make_music(hashcode, vin + vout, size)

    async def _modulate(self, data: json) -> None:
        """
        Change the properties of the MusicMaker.
        """
        ...

    async def _consumer_handler(self, websocket):
        """
        Consume the websocket message and do something with it.
        """
        async for message in websocket:
            data = json.loads(message)
            if data['op'] == 'utx':
                await self._play(data)
            elif data['op'] == 'block':
                await self._modulate(data)


if __name__ == "__main__":
    CM = CryptoMusic()
    CM.start()
